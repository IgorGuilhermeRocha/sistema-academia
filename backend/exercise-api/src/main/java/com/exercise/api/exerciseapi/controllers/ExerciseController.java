package com.exercise.api.exerciseapi.controllers;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.api.exerciseapi.domain.models.Exercise;
import com.exercise.api.exerciseapi.domain.models.dto.ExerciseDto;
import com.exercise.api.exerciseapi.domain.services.ExerciseService;

@RestController
@RequestMapping("/exercises")
public class ExerciseController {

    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping()
    public List<Exercise> findAllExercises() {
        return exerciseService.findAll();
    }

    @GetMapping("/{exerciseId}")
    public ExerciseDto findById(@PathVariable Long exerciseId) {
        return modelMapper.map(exerciseService.findById(exerciseId), ExerciseDto.class);
    }

    @PutMapping("/{exerciseId}")
    public ExerciseDto updatExercise(@PathVariable Long exerciseId, @RequestBody Exercise exercise) {
        exercise.setId(exerciseId);
        return modelMapper.map(exerciseService.updateExercise(exercise), ExerciseDto.class);
    }

    @PostMapping("/create-new-exercise")
    @ResponseStatus(HttpStatus.CREATED)
    public ExerciseDto createNewExercise(@RequestBody Exercise exercise) {
        return modelMapper.map(exerciseService.createNewExercise(exercise), ExerciseDto.class);
    }

    @DeleteMapping("{exerciseId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteExercise(@PathVariable Long exerciseId) {
        exerciseService.deleteExercise(exerciseId);
    }

}
