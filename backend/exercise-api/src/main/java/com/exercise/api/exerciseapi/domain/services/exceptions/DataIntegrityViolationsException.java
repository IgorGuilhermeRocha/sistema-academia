package com.exercise.api.exerciseapi.domain.services.exceptions;

public class DataIntegrityViolationsException extends RuntimeException {
    public DataIntegrityViolationsException(String message) {
        super(message);
    }
}
