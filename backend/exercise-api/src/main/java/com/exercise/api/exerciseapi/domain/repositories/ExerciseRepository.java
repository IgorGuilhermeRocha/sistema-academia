package com.exercise.api.exerciseapi.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.exercise.api.exerciseapi.domain.models.Exercise;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {

    @Query("SELECT obj FROM Exercise obj WHERE obj.name = :name ")
    Exercise findByName(String name);

}
