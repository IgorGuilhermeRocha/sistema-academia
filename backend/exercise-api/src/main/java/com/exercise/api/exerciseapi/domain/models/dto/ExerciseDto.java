package com.exercise.api.exerciseapi.domain.models.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ExerciseDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String muscularGroup;

}
