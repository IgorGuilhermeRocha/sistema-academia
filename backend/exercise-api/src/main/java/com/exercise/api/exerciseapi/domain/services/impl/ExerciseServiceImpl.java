package com.exercise.api.exerciseapi.domain.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.exercise.api.exerciseapi.domain.models.Exercise;
import com.exercise.api.exerciseapi.domain.repositories.ExerciseRepository;
import com.exercise.api.exerciseapi.domain.services.ExerciseService;
import com.exercise.api.exerciseapi.domain.services.exceptions.ObjectNotFoundException;

@Service
public class ExerciseServiceImpl implements ExerciseService {

    @Autowired
    public ExerciseRepository exerciseRepository;

    @Override
    public List<Exercise> findAll() {
        return exerciseRepository.findAll();
    }

    @Override
    public Exercise findById(Long id) {
        return exerciseRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("Id inválido"));
    }

    @Override
    public Exercise createNewExercise(Exercise exercise) {

        if (!VerifyIfNameAlreadyExists(exercise.getName())) {
            return exerciseRepository.save(exercise);
        }

        throw new DataIntegrityViolationException("Nome já cadastrado");

    }

    @Override
    public Exercise updateExercise(Exercise exercise) {
        Exercise opExercise = findById(exercise.getId());
        if (opExercise != null) {
            if (VerifyIfNameAlreadyExists(exercise.getName())) {
                throw new DataIntegrityViolationException("Nome já cadastrado");
            }
            return exerciseRepository.save(exercise);
        }
        throw new ObjectNotFoundException("id inválido");

    }

    @Override
    public Exercise findByName(String name) {
        return exerciseRepository.findByName(name);

    }

    private boolean VerifyIfNameAlreadyExists(String name) {

        Optional<Exercise> opExercise = Optional.ofNullable(findByName(name));
        if (!opExercise.isPresent()) {
            return false;
        }
        return true;
    }

    @Override
    public void deleteExercise(Long id) {
        findById(id);
        exerciseRepository.deleteById(id);
    }

}
