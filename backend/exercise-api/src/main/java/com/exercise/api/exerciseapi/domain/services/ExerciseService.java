package com.exercise.api.exerciseapi.domain.services;

import java.util.List;
import com.exercise.api.exerciseapi.domain.models.Exercise;

public interface ExerciseService {

    public List<Exercise> findAll();

    public Exercise findById(Long id);

    public Exercise createNewExercise(Exercise exercise);

    public Exercise updateExercise(Exercise exercise);

    public Exercise findByName(String name);

    public void deleteExercise(Long id);

}
