package com.exercise.api.exerciseapi.resources.exceptions;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StandartException {

    private LocalDateTime localDateTime;
    private Integer status;
    private String error;
    private String path;

}