package com.client.exercise.api.clientexerciseapi.domain.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientExerciseKey implements Serializable {

    @Column(name = "client_id")
    public Long clientId;

    @Column(name = "exercise_id")
    public Long exerciseId;
}
