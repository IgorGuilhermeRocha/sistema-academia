package com.client.exercise.api.clientexerciseapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.client.exercise.api.clientexerciseapi.domain.models.ClientExercise;
import com.client.exercise.api.clientexerciseapi.domain.services.ClientExerciseService;

@RestController
@RequestMapping("/client-exercise")
public class ClientExerciseController {

    @Autowired
    private ClientExerciseService service;

    @GetMapping("/{email}-{weekDay}")
    public List<ClientExercise> findAllExercisesByWeekDay(@PathVariable String email, @PathVariable String weekDay) {
        return service.selectClientExerciseByWeekDay(email, weekDay);
    }

}
