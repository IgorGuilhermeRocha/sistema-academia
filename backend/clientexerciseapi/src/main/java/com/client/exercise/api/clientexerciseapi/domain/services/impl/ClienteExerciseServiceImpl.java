package com.client.exercise.api.clientexerciseapi.domain.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.client.exercise.api.clientexerciseapi.domain.models.ClientExercise;
import com.client.exercise.api.clientexerciseapi.domain.repositories.ClientExerciseRepository;
import com.client.exercise.api.clientexerciseapi.domain.services.ClientExerciseService;

@Service
public class ClienteExerciseServiceImpl implements ClientExerciseService {

    @Autowired
    private ClientExerciseRepository repository;

    @Override
    public List<ClientExercise> selectClientExerciseByWeekDay(String email, String weekDay) {
        return repository.findClientExerciseByWeekDay(email, weekDay);
    }

}
