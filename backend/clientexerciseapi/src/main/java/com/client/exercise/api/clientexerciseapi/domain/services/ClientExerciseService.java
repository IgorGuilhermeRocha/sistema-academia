package com.client.exercise.api.clientexerciseapi.domain.services;

import java.util.List;

import com.client.exercise.api.clientexerciseapi.domain.models.ClientExercise;

public interface ClientExerciseService {

    public List<ClientExercise> selectClientExerciseByWeekDay(String email, String weekDay);

}
