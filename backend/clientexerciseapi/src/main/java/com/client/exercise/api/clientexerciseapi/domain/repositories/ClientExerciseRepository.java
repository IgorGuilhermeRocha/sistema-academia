package com.client.exercise.api.clientexerciseapi.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.client.exercise.api.clientexerciseapi.domain.models.ClientExercise;
import com.client.exercise.api.clientexerciseapi.domain.models.ClientExerciseKey;

@Repository
public interface ClientExerciseRepository extends JpaRepository<ClientExercise, ClientExerciseKey> {

    @Query("SELECT ce FROM ClientExercise ce where ce.client.email = :email and  ce.weekDay = :weekDay")
    public List<ClientExercise> findClientExerciseByWeekDay(String email, String weekDay);
}
