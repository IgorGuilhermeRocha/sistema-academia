package com.client.exercise.api.clientexerciseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientexerciseapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientexerciseapiApplication.class, args);
	}

}
