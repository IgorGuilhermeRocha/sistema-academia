package com.client.exercise.api.clientexerciseapi.domain.models;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientExercise implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    public ClientExerciseKey id;

    @ManyToOne
    @MapsId("clientId")
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne
    @MapsId("exerciseId")
    @JoinColumn(name = "exercise_id")
    private Exercise exercise;

    private Double weight;
    private Integer sets;
    private Integer reps;
    private Boolean isUnilateral;
    private String weekDay;

}
