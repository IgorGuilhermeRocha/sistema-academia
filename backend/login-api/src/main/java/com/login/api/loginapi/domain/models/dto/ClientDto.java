package com.login.api.loginapi.domain.models.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String lastName;
    private Character gender;
    private Float weight;
    private String email;

}
