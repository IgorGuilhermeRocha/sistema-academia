package com.login.api.loginapi.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.login.api.loginapi.domain.models.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

    public Client findByEmailAndPassword(String email, String password);

    public Client findByEmail(String email);
}
