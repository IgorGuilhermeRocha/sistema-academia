package com.login.api.loginapi.domain.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.login.api.loginapi.domain.models.Client;
import com.login.api.loginapi.domain.repositories.ClientRepository;
import com.login.api.loginapi.domain.services.ClientService;
import com.login.api.loginapi.domain.services.exceptions.DataIntegratyViolationException;
import com.login.api.loginapi.domain.services.exceptions.ObjectNotFoundException;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> listAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client login(String email, String password) {
        Optional<Client> opClient = Optional.ofNullable(clientRepository.findByEmailAndPassword(email, password));
        return opClient.orElseThrow(() -> new ObjectNotFoundException(
                "Email, ou senha estão incorreto(s)."));
    }

    @Override
    public Client createNewClient(Client client) {
        Client clientAux = findClientByEmail(client.getEmail());
        if (clientAux != null) {
            throw new DataIntegratyViolationException("Este email já esta em uso.");
        }
        return clientRepository.save(client);
    }

    @Override
    public Client updateClient(Client client) {
        findById(client.getId());
        Client clientAux = findClientByEmail(client.getEmail());
        if (clientAux != null) {
            throw new DataIntegratyViolationException("Este email já esta em uso.");
        }
        return clientRepository.save(client);

    }

    @Override
    public Client findById(Long clientId) {
        Optional<Client> opClient = clientRepository.findById(clientId);
        return opClient.orElseThrow(() -> new ObjectNotFoundException("Este usuário não existe."));

    }

    @Override
    public void deleteClient(Long clientId) {
        findById(clientId);
        clientRepository.deleteById(clientId);

    }

    private Client findClientByEmail(String email) {
        Optional<Client> opCLient = Optional.ofNullable(clientRepository.findByEmail(email));
        return opCLient.orElse(null);
    }

}
