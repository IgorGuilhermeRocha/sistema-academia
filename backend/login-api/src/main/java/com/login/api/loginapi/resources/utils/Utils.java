package com.login.api.loginapi.resources.utils;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import com.login.api.loginapi.domain.models.Client;
import com.login.api.loginapi.domain.models.dto.ClientDto;

public class Utils {

    public static List<ClientDto> changeTypeOfList(List<Client> listClient, ModelMapper modelMapper) {
        List<ClientDto> listClientDto = new ArrayList();
        for (Client client : listClient) {
            listClientDto.add(modelMapper.map(client, ClientDto.class));
        }
        return listClientDto;
    }

}
