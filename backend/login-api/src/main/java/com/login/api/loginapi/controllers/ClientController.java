package com.login.api.loginapi.controllers;

import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.login.api.loginapi.domain.models.Client;
import com.login.api.loginapi.domain.models.dto.ClientDto;
import com.login.api.loginapi.domain.services.ClientService;
import com.login.api.loginapi.resources.utils.Utils;

@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    public ModelMapper mapper;

    @Autowired
    public ClientService clientService;

    @GetMapping()
    public List<ClientDto> listAllClients() {
        return Utils.changeTypeOfList(clientService.listAllClients(), mapper);
    }

    @PostMapping("/login")
    public ClientDto login(@RequestBody Client client) {
        ClientDto clientDto = mapper.map(clientService.login(client.getEmail(), client.getPassword()), ClientDto.class);
        return clientDto;
    }

    @PutMapping("{clientId}")
    public ClientDto updateClient(@PathVariable Long clientId, @RequestBody Client client) {
        client.setId(clientId);
        ClientDto clientDto = mapper.map(clientService.updateClient(client), ClientDto.class);
        return clientDto;

    }

    @PostMapping("/create-new-client")
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDto createNewClient(@RequestBody Client client) {
        ClientDto clientDto = mapper.map(clientService.createNewClient(client), ClientDto.class);
        return clientDto;

    }

    @DeleteMapping("/{clientId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteClient(@PathVariable Long clientId) {
        clientService.deleteClient(clientId);
    }

}
