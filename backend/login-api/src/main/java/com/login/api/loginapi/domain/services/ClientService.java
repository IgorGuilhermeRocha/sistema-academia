package com.login.api.loginapi.domain.services;

import java.util.List;

import com.login.api.loginapi.domain.models.Client;

public interface ClientService {

    public Client login(String email, String password);

    public Client createNewClient(Client client);

    public Client updateClient(Client client);

    public List<Client> listAllClients();

    public Client findById(Long clientId);

    public void deleteClient(Long clientId);

}
