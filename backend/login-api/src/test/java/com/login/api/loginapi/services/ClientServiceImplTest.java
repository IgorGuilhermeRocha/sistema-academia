package com.login.api.loginapi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.login.api.loginapi.ConfigTest;
import com.login.api.loginapi.domain.models.Client;
import com.login.api.loginapi.domain.repositories.ClientRepository;
import com.login.api.loginapi.domain.services.exceptions.ObjectNotFoundException;
import com.login.api.loginapi.domain.services.impl.ClientServiceImpl;

@SpringBootTest
public class ClientServiceImplTest extends ConfigTest {

    private static final long id = 1;
    private static final String name = "Igor";
    private static final String lastName = "Rocha";
    private static final char gender = 'M';
    private static final float weigth = 74.5f;
    private static final String email = "igorguilhermerocha@hotmail.com";
    private static final String password = "123";

    private Client client;
    private Optional<Client> opClient;

    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private ClientRepository clientRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.setUpVariables();
    }

    @Test
    public void whenEmailAndPasswordMatchMustBeReturnAClientInstance() {
        Mockito.when(clientRepository.findByEmailAndPassword("igorguilhermedev@gmail.com", "123")).thenReturn(client);
        Client opClient = clientService.login("igorguilhermedev@gmail.com", "123");
        assertEquals(opClient.getId(), client.getId());
        assertEquals(opClient.getName(), client.getName());
        assertEquals(opClient.getLastName(), client.getLastName());
        assertEquals(opClient.getGender(), client.getGender());
        assertEquals(opClient.getEmail(), client.getEmail());
        assertEquals(opClient.getPassword(), client.getPassword());

    }

    @Test

    public void whenIdDontMatchMustBeReturnAnException() {
        Mockito.when(clientRepository.findById(1L)).thenReturn(opClient);
        Client opClientAux = clientService.findById(1L);
        assertEquals(opClientAux.getName(), "Igor");

    }

    @Test
    public void whenEmailAndPasswordDontMatchMustBeReturnAObjectNotFound() {
        Mockito.when(clientRepository.findByEmailAndPassword(Mockito.anyString(),
                Mockito.anyString())).thenThrow(new ObjectNotFoundException("Email ou senha incorreto(s)"));

        try {
            clientService.login("idsd", "null");
        } catch (Exception e) {
            assertEquals(e.getClass(), ObjectNotFoundException.class);
            assertEquals("Email ou senha incorreto(s)", e.getMessage());
        }
    }

    private void setUpVariables() {
        this.client = new Client(id, name, lastName, gender, weigth, email, password);
        opClient = Optional.ofNullable(client);

    }

}
