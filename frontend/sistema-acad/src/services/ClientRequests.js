import axios from "axios";
import { ClientBaseUrl } from "../helpers/ClientBaseUrl";
import { ClientExerciseUrl } from "../helpers/ClientExerciseUrl";
import { createErrorWindow } from "../helpers/ErrorWindows";
import { Navigate } from "react-router-dom";

export function login(client) {

    let url = ClientBaseUrl + "/login";

    axios(url, {
        method: "POST",
        data: JSON.stringify(client),
        headers: {
            "Content-Type": "application/json"
        }
    }).then((response) => loginResponseHandler({ response })).catch((error) => createErrorWindow(error));

}

function loginResponseHandler({ response }) {
    if (response.status >= 400) {
        createErrorWindow(response.data);

    } else {
        <Navigate to="/exercicios" />
    }
}

export function createNewAccount(client) {
    let url = ClientBaseUrl + "/create-new-client";

    axios(url, {
        method: "POST",
        data: JSON.stringify(client),
        headers: {
            "Content-Type": "application/json"
        }
    }).then((response) => createNewAccountResponseHandler({ response })).catch((error) => createErrorWindow(error));

}

function createNewAccountResponseHandler({ response }) {
    if (response.status >= 400) {
        createErrorWindow(response.data);
    }

}

export function getExercisesByWeekDay(email, weekDay) {
    let url = ClientExerciseUrl + "/" + email + "-" + weekDay;
    console.log(url);
}