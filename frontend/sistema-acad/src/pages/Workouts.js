import "../styles/workouts/workouts.css";
import "../styles/workouts/components/side_menu.css";
import SideMenu from "../components/SideMenu";
import TableOfExercises from "../components/TableOfExercises";

function Workouts() {
    return (
        <div className="main_content">
            <SideMenu />
            <TableOfExercises />
        </div>
    );
}

export default Workouts;