import Form from "../components/Form";
import Logo from "../components/Logo";
import "../styles/index/index.css";

function Index() {

    return (
        <div className="main_content">
            <h1>Performace academia </h1>
            <div className="logo_and_form">
                <Logo />
                <Form />
            </div>
        </div>
    );
}

export default Index;
