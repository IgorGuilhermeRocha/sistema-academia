import Index from "../../pages/Index";
import Workouts from "../../pages/Workouts";
import { Routes, Route } from "react-router-dom";

export default function switchPages() {
    return (
        <Routes>
            <Route exact path="/" element={<Index />} />
            <Route exact path="/exercises" element={<Workouts />} />
        </Routes>
    );
}