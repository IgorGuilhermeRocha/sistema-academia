import "../styles/index/components/form.css";
import LoginForm from "./LoginForm";
import { useState } from "react";
import SingInForm from "./SingInForm";

function Form() {

    const [value, setValue] = useState("1");

    return (
        <>
            {value && (
                <>
                    <LoginForm setValue={setValue} />
                </>)}

            {!value && (
                <>
                    <SingInForm setValue={setValue} />
                </>
            )}

        </>
    );
}

export default Form;

