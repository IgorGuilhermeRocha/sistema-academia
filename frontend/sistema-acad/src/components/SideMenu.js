import Logo from "../assets/imgs/logo.png";
function SideMenu() {

    return (
        <div className="side_menu">
            <div className="logo">
                <img src={Logo} alt="" />
            </div>
            <div className="week_days">

                <span onClick={setTableValue}>Segunda-feira</span>
                <span onClick={setTableValue}>Terça-feira</span>
                <span onClick={setTableValue}>Quarta-feira</span>
                <span onClick={setTableValue}>Quinta-feira</span>
                <span onClick={setTableValue}>Sexta-feira</span>
                <span onClick={setTableValue}>Sábado</span>

            </div>
        </div>);
}

export default SideMenu;

function setTableValue(e) {
    console.log(e.target.innerText);

}