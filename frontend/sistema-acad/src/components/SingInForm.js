import { useForm } from "react-hook-form";
import { createNewAccount } from "../services/ClientRequests"

function SingInForm(props) {
    const { register, handleSubmit } = useForm();

    return (

        <form onSubmit={handleSubmit(submitForm)}>
            <div className="name">
                <label htmlFor="name">Nome</label>
                <input id="name" type="text" {...register('name', { required: true })} />
            </div>

            <div className="last_name">
                <label htmlFor="last_name">Sobrenome</label>
                <input id="last_name" type="text" {...register('lastName', { required: true })} />
            </div>

            <div className="weight">
                <label htmlFor="weight">Peso</label>
                <input id="weight" type="number" {...register('weight', { required: true })} />
            </div>

            <div className="gender">
                <label htmlFor="gender"> Gênero </label>
                <select {...register('gender', { required: true })}>
                    <option value="M">M</option>
                    <option value="F">F</option>
                    <option value="O">O</option>
                </select>
            </div>

            <div className="email">
                <label htmlFor="email">Email</label>
                <input id="email" type="text" {...register('email', { required: true })} />
            </div>

            <div className="password">
                <label htmlFor="password">Senha</label>
                <input id="password" type="password" {...register('password', { required: true })} />
            </div>

            <div className="buttons">
                <button type="submit" >Criar</button>
                <button type="button" onClick={() => props.setValue("1")} >Voltar</button>
            </div>
        </form>

    );
}

export default SingInForm;

function submitForm(data) {
    createNewAccount(data);
}