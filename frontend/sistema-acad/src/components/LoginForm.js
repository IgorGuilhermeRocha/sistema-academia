import { login } from "../services/ClientRequests";
import { useForm } from "react-hook-form";


function LoginForm(props) {

    const { register, handleSubmit } = useForm();

    return (

        <form className="form" onSubmit={handleSubmit(submitForm)}>
            <div className={"email"}>
                <label htmlFor="email">Email</label>
                <input id="email" type="text" {...register('email', { required: true })} />
            </div>

            <div className="password">
                <label htmlFor="password">Senha</label>
                <input id="password" type="password" {...register('password', { required: true })} />
            </div>

            <div className="buttons">
                <button type="submit"> Entrar </button>
                <button type="button" onClick={() => {
                    props.setValue("");
                }} > Não tem uma conta ? </button>
            </div>
        </form>

    );
}

export default LoginForm;

function submitForm(data) {
    login(data);
}

