import logo from "../assets/imgs/logo.png";
function Logo() {

    return (
        <div className="logo">
            <img src={logo} alt="" />
        </div>
    );
}

export default Logo;
